*Wish List*

This is a program where you can manage the products you wish to buy

*Definition*

This is a project done for the course User Interfaces. 
It is done using the framework flutter, which I'm learning.
This project was done with the intention of learning how to use redux as the app's state management
and to fulfill the course project specification (developing an interface and testing how it was used).