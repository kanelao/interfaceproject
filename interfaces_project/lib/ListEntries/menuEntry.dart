import 'package:flutter/material.dart';

class MenuEntry extends StatelessWidget {
  final IconData icon;
  final String text;
  final Function onPressed;

  MenuEntry(
      {@required this.icon, @required this.text, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        onPressed();
      },
      child: Container(
        child: Row(
          children: <Widget>[
            Icon(icon),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  text,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ))
          ],
        ),
      ),
    );
  }
}
