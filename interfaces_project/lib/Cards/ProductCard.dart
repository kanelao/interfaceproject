import 'dart:io';

import 'package:flutter/material.dart';
import '../Classes/product_entry.dart';

typedef ChangeFavorite = void Function(int index);
typedef RemoveProduct = void Function(int index);
typedef InformationPage = void Function(int index);

class ProductCard extends StatelessWidget {
  final ProductEntry productEntry;
  final ChangeFavorite changeFavorite;
  final RemoveProduct removeProduct;
  final InformationPage informationPage;
  final int index;
 final String imagePlaceholder =
      'https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F0%2F0c%2FVitellaria_paradoxa_MS_6563.JPG%2F1200px-Vitellaria_paradoxa_MS_6563.JPG&f=1';

  ProductCard({
    @required this.productEntry,
    @required this.index,
    @required this.changeFavorite,
    @required this.removeProduct,
    @required this.informationPage
  });


  Widget displayImage() {
    if (productEntry.product.imageLocation.isEmpty) {
      return Image.network(imagePlaceholder);
    }
    return Container(
        height: 200,
        child:
            Image(image: FileImage(File(productEntry.product.imageLocation)), fit: BoxFit.contain));
  }


  Widget _actionRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
          icon: Icon(
            productEntry.isFavourite ? Icons.favorite : Icons.favorite_border,
            color: productEntry.isFavourite ? Colors.pink : Colors.blueGrey,
          ),
          onPressed: () {
            changeFavorite(index);
          },
        ),
        IconButton(
          icon: Icon(
            Icons.info,
            color: Colors.blueGrey,
          ),
          onPressed: () {
            informationPage(index);
          },
        ),
        IconButton(
          icon: Icon(
            Icons.delete,
            color: Colors.red,
          ),
          onPressed: () {
            removeProduct(index);
          },
        ),
      ],
    );
  }

  Widget _informationRow() {
    return Container(
      padding: EdgeInsets.only(top: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            productEntry.product.name,
            style: TextStyle(
                fontSize: 25,
                color: Colors.blueGrey,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5), color: Colors.pink),
            child: Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "${productEntry.product.price.toString()}€",
                  style: TextStyle(color: Colors.white),
                )),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          displayImage(),
          _informationRow(),
          Container(
            width: 50,
            height: 2,
            color: Colors.blue,
            margin: EdgeInsets.only(top: 10, bottom: 5),
          ),
          _actionRow()
        ],
      ),
    );
  }
}
