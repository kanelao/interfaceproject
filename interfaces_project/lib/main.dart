import 'package:flutter/material.dart';
import 'package:interfaces_project/Classes/product.dart';
import 'package:interfaces_project/Data/products.dart';
import 'package:interfaces_project/Pages/productInfoPage.dart';
import 'package:interfaces_project/Pages/productPage.dart';
import 'package:interfaces_project/Pages/tabViewPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Database database = Database();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: TabViewPage(database: database),
        onGenerateRoute: (RouteSettings route) {
          final pathElements = route.name.split('/');
          if (pathElements[0] != '') return null;
          if (pathElements[1] != 'createProduct' &&
              pathElements[1] != 'productInformationPage' &&
              pathElements[1] != 'editProduct') return null;
          switch (pathElements[1]) {
            case 'createProduct':
              return MaterialPageRoute<bool>(builder: (BuildContext context) {
                return ProductPage(
                  add: database.addProduct,
                  product: Product.create(),
                );
              });
            case 'productInformationPage':
              return MaterialPageRoute<bool>(builder: (BuildContext context) {
                return ProductInfoPage(
                  index: int.parse(pathElements[2]),
                  productEntry: database.getProduct(
                    int.parse(pathElements[2]),
                  ),
                );
              });
            case 'editProduct':
              return MaterialPageRoute<bool>(builder: (BuildContext context) {
                return ProductPage(
                  edit: database.editProduct,
                  index: int.parse(pathElements[2]),
                  product: database
                      .getProduct(
                        int.parse(pathElements[2]),
                      )
                      .product,
                );
              });
          }
        });
  }
}
