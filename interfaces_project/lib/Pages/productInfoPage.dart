import 'dart:io';

import 'package:flutter/material.dart';
import 'package:interfaces_project/Classes/product_entry.dart';

class ProductInfoPage extends StatelessWidget {
  final ProductEntry productEntry;
  final int index;
  final String imagePlaceholder =
      'https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F0%2F0c%2FVitellaria_paradoxa_MS_6563.JPG%2F1200px-Vitellaria_paradoxa_MS_6563.JPG&f=1';

  ProductInfoPage({
    @required this.productEntry,
    @required this.index,
  });

  Widget displayImage() {
    if (productEntry.product.imageLocation.isEmpty) {
      return Image.network(imagePlaceholder);
    }
    return Container(
        height: 200,
        child: Image(
            image: FileImage(File(productEntry.product.imageLocation)),
            fit: BoxFit.contain));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(productEntry.product.name),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.pushNamed(context, '/editProduct/$index');
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(8),
            height: 200,
            decoration: BoxDecoration(
              boxShadow: [BoxShadow(offset: Offset(0, 0.5), blurRadius: 5)],
            ),
            child: displayImage(),
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                productEntry.product.name,
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5), color: Colors.pink),
                child: Container(
                    padding: EdgeInsets.all(5),
                    child: Text(
                      "${productEntry.product.price.toString()}€",
                      style: TextStyle(color: Colors.white),
                    )),
              ),
            ],
          ),
          Container(
            width: 50,
            height: 2,
            color: Colors.blue,
            margin: EdgeInsets.only(top: 10, bottom: 30),
          ),
          Padding(
              padding: EdgeInsets.all(5),
              child: Text(
                productEntry.product.description,
                textAlign: TextAlign.justify,
              ))
        ],
      ),
    );
  }
}
