import 'package:flutter/material.dart';
import 'package:interfaces_project/Cards/ProductCard.dart';
import 'package:interfaces_project/Classes/product_entry.dart';

typedef RemoveProduct = void Function(int index);

class WishListPage extends StatefulWidget {
  final List<ProductEntry> products;
  final RemoveProduct removeProduct;

  WishListPage({@required this.products, @required this.removeProduct});

  @override
  _WishListPageState createState() => _WishListPageState();
}

class _WishListPageState extends State<WishListPage> {
  bool isFavorites = false;

  void changeFavorite(int index) {
    setState(() {
      widget.products[index].isFavourite = !widget.products[index].isFavourite;
    });
  }

  void informationPage(int index) {
    Navigator.pushNamed(context, '/productInformationPage/$index');
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView.builder(
          itemCount: widget.products.length,
          itemBuilder: (BuildContext context, int index) {
            if ((isFavorites && widget.products[index].isFavourite) ||
                !isFavorites)
              return ProductCard(
                productEntry: widget.products[index],
                changeFavorite: changeFavorite,
                index: index,
                removeProduct: widget.removeProduct,
                informationPage: informationPage,
              );
            return null;
          },
        ),
        Positioned(
          bottom: 10,
          right: 10,
          child: FloatingActionButton(
            heroTag: 'create',
            tooltip: 'Add wish',
            onPressed: () {
              Navigator.pushNamed(context, '/createProduct');
            },
            child: Icon(Icons.add),
          ),
        ),
        Positioned(
          bottom: 75,
          right: 20,
          child: Container(
            width: 40,
            height: 40,
            child: Tooltip(
              message: isFavorites ? 'Show all' : 'Show favorites',
              verticalOffset: -60,
              child: FloatingActionButton(
                heroTag: 'favorite',
                backgroundColor: Colors.pink,
                onPressed: () {
                  setState(() {
                    isFavorites = !isFavorites;
                  });
                },
                child: Icon(
                  isFavorites ? Icons.favorite : Icons.favorite_border,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
