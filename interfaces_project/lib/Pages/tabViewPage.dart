import 'package:flutter/material.dart';
import 'package:interfaces_project/Pages/statusPage.dart';
import 'package:interfaces_project/Pages/wishListPage.dart';
import '../Data/products.dart';

class TabViewPage extends StatefulWidget {
  final Database database;

  TabViewPage({@required this.database});
  @override
  _TabViewPageState createState() => _TabViewPageState();
}

class _TabViewPageState extends State<TabViewPage> {
  double budget = 20.0;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  Widget mainPage() {
    return Drawer(
      child: StatusPage(
        budget: budget,
        used: widget.database.totalPrice(),
        changeBudget: changeBudget,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: mainPage(),
      appBar: AppBar(
        title: Center(
          child: Text('Wish Manager'),
        ),
        leading: IconButton(
          icon: Icon(Icons.assessment),
          onPressed: () {
            scaffoldKey.currentState.openDrawer();
          },
        ),
      ),
      body: WishListPage(
        products: widget.database.products,
        removeProduct: removeProduct,
      ),
    );
  }

  void changeBudget(double budget) {
    setState(() {
      this.budget = budget;
    });
  }

  void removeProduct(int index) {
    setState(() {
      widget.database.removeProduct(index);
    });
  }
}
