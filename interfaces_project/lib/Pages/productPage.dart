import 'dart:io';

import 'package:flutter/material.dart';
import '../Classes/product.dart';
import 'package:image_picker/image_picker.dart';

typedef AddProduct = void Function(Product product);
typedef EditProduct = void Function(int index, Product product);

class ProductPage extends StatefulWidget {
  final int index;
  final Product product;
  final AddProduct add;
  final EditProduct edit;

  ProductPage(
      {@required this.product,
      this.add = null,
      this.index = -1,
      this.edit = null});

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  TextEditingController nameController;
  TextEditingController descriptionController;
  TextEditingController priceController;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String name;
  String description;
  double price;
  String imageLocation = '';
  final String imagePlaceholder =
      'https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F0%2F0c%2FVitellaria_paradoxa_MS_6563.JPG%2F1200px-Vitellaria_paradoxa_MS_6563.JPG&f=1';

  @override
  void initState() {
    super.initState();
    name = widget.product.name;
    description = widget.product.description;
    price = widget.product.price;
  }

  Widget displayImage() {
    if (imageLocation.isEmpty) {
      return Image.network(imagePlaceholder);
    }
    return Container(
      height: 200,
      child: Image(image: FileImage(File(imageLocation)), fit: BoxFit.contain)
      );
  }

  Widget image() {
    return Stack(
      alignment: FractionalOffset.bottomRight,
      children: <Widget>[
        displayImage(),
        Padding(
          padding: EdgeInsets.all(8),
          child: FloatingActionButton(
            backgroundColor: Colors.white,
            child: Icon(
              Icons.add_a_photo,
              color: Colors.black,
            ),
            onPressed: getImage,
          ),
        )
      ],
    );
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageLocation = image.path;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(child: Text('Add Product')),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              image(),
              TextFormField(
                controller: nameController,
                initialValue: name,
                decoration: InputDecoration(labelText: 'Product Name'),
                validator: (String value) {
                  if (value.isEmpty) return 'Name cannot be empty';
                },
                onSaved: (String value) {
                  name = value;
                },
              ),
              TextFormField(
                controller: descriptionController,
                initialValue: description,
                decoration: InputDecoration(labelText: 'Product Description'),
                maxLines: 4,
                validator: (String value) {
                  if (value.isEmpty) return 'Description cannot be empty';
                },
                onSaved: (String value) {
                  description = value;
                },
              ),
              TextFormField(
                controller: priceController,
                initialValue: price.toString(),
                decoration: InputDecoration(labelText: 'Product Price'),
                keyboardType: TextInputType.number,
                validator: (String value) {
                  if (value.isEmpty ||
                      !RegExp(r'^(?:[1-9]\d*|0)?(?:[.,]\d+)?$').hasMatch(value))
                    return 'Price is required and should be a number';
                },
                onSaved: (String value) {
                  price = double.parse(value);
                },
              ),
              RaisedButton(
                color: Colors.blue,
                child: Text(
                  'Save',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  if (!_formKey.currentState.validate()) return;
                  _formKey.currentState.save();
                  final Product product = Product(
                      description: description, name: name, price: price, imageLocation: imageLocation);
                  if (widget.index < 0) {
                    widget.add(product);
                    Navigator.pop(context);
                  }
                  widget.edit(widget.index, product);
                  Navigator.pop(context);
                  Navigator.pushReplacementNamed(
                      context, '/productInformationPage/${widget.index}');
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
