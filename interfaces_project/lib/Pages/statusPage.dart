import 'dart:math';

import 'package:flutter/material.dart';

typedef ChangeBudget = void Function(double budget);

class StatusPage extends StatelessWidget {
  final double budget;
  final double used;
  final TextEditingController newBudget = TextEditingController();
  final ChangeBudget changeBudget;
  final GlobalKey<FormState> _priceKey = GlobalKey<FormState>();

  StatusPage(
      {@required this.budget,
      @required this.used,
      @required this.changeBudget});

  Widget _balance(double remaining) {
    Color textColor = remaining < 0 ? Colors.redAccent : Colors.green;
    return Column(
      children: [
        Text(
          '$remaining',
          style: TextStyle(
              color: textColor, fontWeight: FontWeight.bold, fontSize: 23),
        ),
        Container(
          width: 70,
          height: 4,
          color: remaining < 0 ? Colors.red : Colors.green,
          margin: EdgeInsets.only(top: 5, bottom: 5),
        ),
        Text(
          'Balance',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
      ],
    );
  }

  Widget _budget() {
    return Column(
      children: [
        Text('$budget',
            style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold)),
        Container(
          width: 50,
          height: 2,
          color: Colors.blue,
          margin: EdgeInsets.only(top: 5, bottom: 5),
        ),
        Text(
          'Budget',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  Widget _used() {
    return Column(
      children: [
        Text('$used',
            style: TextStyle(color: Colors.pink, fontWeight: FontWeight.bold)),
        Container(
          width: 50,
          height: 2,
          color: Colors.pink,
          margin: EdgeInsets.only(top: 5, bottom: 5),
        ),
        Text(
          'Total',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double remaining = budget - used;
    return Stack(children: [
      Container(
        height: 80,
        child: AppBar(
          title: Text('Status'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      ),
      Center(
        child: CustomPaint(
          painter: StatusPainter(
              value1: budget, value: used, screen: MediaQuery.of(context).size),
        ),
      ),
      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.symmetric(vertical: 8),
            child: _balance(remaining)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _budget(),
            _used(),
          ],
        ),
      ]),
      Positioned(
        bottom: 8,
        right: 8,
        child: RaisedButton(
          color: Colors.blue,
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('What is the new budget'),
                    content: Form(
                      key: _priceKey,
                      child: TextFormField(
                        keyboardType: TextInputType.numberWithOptions(),
                        decoration: InputDecoration(labelText: 'New Budget'),
                        controller: newBudget,
                        validator: (String value) {
                          if (value.isEmpty ||
                              !RegExp(r'^(?:[1-9]\d*|0)?(?:[.,]\d+)?$')
                                  .hasMatch(value))
                            return 'Price is required and should be a number';
                        },
                        onSaved: (String value) {
                          changeBudget(double.parse(value));
                        },
                      ),
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text('Cancel'),
                      ),
                      FlatButton(
                        onPressed: () {
                          if (!_priceKey.currentState.validate()) return;
                          _priceKey.currentState.save();
                          Navigator.of(context).pop();
                          newBudget.clear();
                        },
                        child: Text('Change'),
                      )
                    ],
                  );
                });
          },
          child: Text(
            remaining < 0 ? 'Increase Budget' : 'Change Budget',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    ]);
  }
}

class StatusPainter extends CustomPainter {
  final double value1;
  final double value;
  Size screen;

  StatusPainter({@required this.value, @required this.value1, this.screen});

  @override
  void paint(Canvas canvas, Size size) {
    screen = screen.height == 0 ? Size(100, 100) : screen;
    double square =
        screen.width < screen.height ? screen.width / 3 : screen.height / 3;
    Paint red = Paint()
      ..color = Colors.pink
      ..strokeWidth = 15.0
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    Paint blue = Paint()
      ..color = Colors.blue
      ..strokeWidth = 15.0
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    Rect rect = Rect.fromLTRB(-square, -square, square, square);
    // Paint green =
    double total = 2 * pi - 0.3;
    if (value1 > value) {
      canvas.drawArc(rect, 0, total, false, blue);
      canvas.drawArc(rect, 0, total * value / value1, false, red);
    } else {
      canvas.drawArc(rect, 0, total, false, red);
      canvas.drawArc(rect, 0, total * value1 / value, false, blue);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
