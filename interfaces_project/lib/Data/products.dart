import 'dart:convert';

import 'package:interfaces_project/Classes/product.dart';
import '../Classes/product_entry.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class Database {
  final String server =
      'https://interfacesproject-2bc28.firebaseio.com/products.json';

  List<ProductEntry> products = [
    ProductEntry(
      id: 'sdsd',
      isFavourite: true,
      product: Product(
          name: "Finish Food", description: 'Food made in Finland', price: 0.0),
    )
  ];
  double total = 0.0;

  Database(){
    getProducts();
  }

  addProduct(Product product) {
    final Map<String, dynamic> newProduct = {
      'name': product.name,
      'description': product.description,
      'imageLocation': product.imageLocation,
      'price': product.price,
      'isFavourite': false,
    };
    return http
        .post(server, body: json.encode(newProduct))
        .then((http.Response response) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      products.add(ProductEntry(
          id: responseData['name'], product: product, isFavourite: false));
    });
  }

  getProducts() {
    http.get(server).then((http.Response response) {
      final List<ProductEntry> fetchedProductList = [];
      final Map<String, dynamic> productListData = json.decode(response.body);
      if (productListData != null && productListData.length > 0)
        productListData.forEach((String key, dynamic productData) {
          final ProductEntry productEntry = ProductEntry(
            id: key,
            isFavourite: productData['isFavorite'],
            product: Product(
              name: productData['name'],
              description: productData['description'],
              imageLocation: productData['imageLocation'],
              price: productData['price'],
            ),
          );
          fetchedProductList.add(productEntry);
        });
      products = fetchedProductList;
    });
    return products;
  }

  getProduct(int index) {
    return products[index];
  }

  removeProduct(int index) {
    http
        .delete(server + '${products[index].id}')
        .then((http.Response response) {
      products.removeAt(index);
    });
  }

  Future<Null> editProduct(int index, Product product) {
    final ProductEntry oldProductEntry = products[index];
    final ProductEntry editedProduct = ProductEntry(
        id: oldProductEntry.id,
        product: product,
        isFavourite: oldProductEntry.isFavourite);

    final Map<String, dynamic> updateProduct = {
      'name': product.name,
      'description': product.description,
      'imageLocation': product.imageLocation,
      'price': product.price,
      'isFavourite': editedProduct.isFavourite,
    };
    return http
        .put(server + '${editedProduct.id}', body: json.encode(updateProduct))
        .then((http.Response response) {
      products.removeAt(index);
      products.insert(index, editedProduct);
    });
  }

  double totalPrice() {
    double total = 0.0;
    for (int i = 0; i < products.length; i++) {
      total += products[i].product.price;
    }
    return total;
  }
}
