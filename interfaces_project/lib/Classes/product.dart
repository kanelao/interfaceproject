import 'package:flutter/material.dart';

class Product {
  String name;
  String description;
  double price;
  String imageLocation;

  Product({
    @required this.name,
    @required this.description,
    @required this.price,
    this.imageLocation = '',
  });

  Product.create() {
    this.name = "";
    this.description = "";
    this.price = 0;
    this.imageLocation = '';
  }
}
