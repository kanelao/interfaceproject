import 'package:flutter/material.dart';
import './product.dart';

class ProductEntry{
  Product product;
  bool isFavourite;
  String id;

  ProductEntry({
    @required this.product,
    this.isFavourite = false,
    @required this.id,
  });
}