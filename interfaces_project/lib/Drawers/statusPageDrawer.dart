import 'package:flutter/material.dart';
import 'package:interfaces_project/Drawers/basicDrawer.dart';
import 'package:interfaces_project/ListEntries/menuEntry.dart';

class StatusPageDrawer extends StatelessWidget {
  final Row drawerChilds = Row(
    children: <Widget>[
      MenuEntry(
        text: 'WishList',
        icon: Icons.pages,
        onPressed: (){},
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return BasicDrawer(
      drawerName: 'Menu',
      drawerChilds: [drawerChilds],
    );
  }
}
