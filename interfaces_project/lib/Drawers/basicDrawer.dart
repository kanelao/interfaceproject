import 'package:flutter/material.dart';

class BasicDrawer extends StatelessWidget {
  final List<Widget> drawerChilds;
  final String drawerName;

  BasicDrawer({
    @required this.drawerChilds,
    this.drawerName = 'Drawer Name',
  });

  @override
  Widget build(BuildContext context) {
    drawerChilds.insert(0, 
    AppBar(
      title: Text('Menu'),
    )
    );
    return Drawer(
      child: Column(
        children: drawerChilds,
      ),
    );
  }
}
